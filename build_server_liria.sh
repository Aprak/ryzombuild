#!/bin/bash
# https://ryzomcore.atlassian.net/wiki/display/RC/CMake+Options
# https://ryzomcore.atlassian.net/wiki/display/RC/Configure+Linux+Server

JDEFAULT=12                     # Change this to the default number of cores in
								# build-system + 50%
export DISTCC_HOSTS='localhost' # If distcc is used, set this to the hosts in
								# cluster
TS=$(date "+%Y-%m-%d.%H.%M.%S")
LOG_FILE=/tmp/ryzon_server_build_liria.log
if [ -f $LOG_FILE ]; then
	OLDLOG=${LOG_FILE}.${TS}
	mv $LOG_FILE $OLDLOG
fi
JN=${1-${JDEFAULT}}

cd $(dirname $0)
RROOT=$(pwd)
cd $OLDPWD

cmake \
	-DWITH_RYZOM_SERVER=ON \
	-DWITH_RYZOM_CLIENT=OFF \
	-DFINAL_VERSION=OFF \
	-DWITH_QT=OFF \
	-DWITH_STATIC=ON \
	-DWITH_STATIC_DRIVERS=ON \
	-DWITH_DRIVER_OPENGL=OFF \
	-DWITH_DRIVER_OPENAL=OFF \
	-DWITH_NEL_TESTS=OFF \
	-DWITH_NEL_TOOLS=OFF \
	-DWITH_NEL_SAMPLES=OFF \
	-DCMAKE_CXX_COMPILER="${RROOT}/g++" \
	-DCMAKE_C_COMPILER="${RROOT}/gcc" \
	../code/

test $? && ( time VERBOSE=1 make -j${JB} 2>&1) | tee ${LOG_FILE}
test -z $OLDLOG && echo "Old log-file: $OLDLOG"
echo
echo "New log-file: $LOG_FILE"
echo "If you have grcat, look at log like this:"
echo "cat $LOG_FILE | grcat conf.gcc | cat -rS"

#	-DWITH_RYZOM_TOOLS=OFF \
#	-DWITH_NEL_TOOLS=OFF \

# Possible settings:
# ========================
#	-DWITH_NEL_SAMPLES=OFF \
#	-DWITH_NEL_TOOLS=OFF \
#	-DWITH_RYZOM_CLIENT=OFF \
#	-DWITH_RYZOM_SERVER=OFF \
#	-DWITH_RYZOM_TOOLS=OFF \
#	-DWITH_STATIC_DRIVERS=ON \
#	-DWITH_SYMBOLS=ON \
#	-DWITH_NEL_TESTS=OFF \
#	-DCMAKE_BUILD_TYPE=Debug \
#
#	-DWITH_LUA51=OFF \
#
#	-DWITH_NEL=ON \
#	-DWITH_SOUND=OFF \
#
# Server related:
# ===============
#	-DFINAL_VERSION=ON \
#	-DWITH_STATIC=ON \
#	-DWITH_QT=OFF \
#
#	-DWITH_DRIVER_OPENGL=OFF \
#	-DWITH_DRIVER_OPENAL=OFF \
