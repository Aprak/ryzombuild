#!/bin/bash
# https://ryzomcore.atlassian.net/wiki/display/RC/CMake+Options
#https://ryzomcore.atlassian.net/wiki/display/RC/Configure+Linux+Server
LOG_FILE=/tmp/ryzon_server_build.log
#time make -j12
#exit $?

cmake \
	-DWITH_RYZOM_CLIENT=OFF \
	-DWITH_NEL=ON \
	-DWITH_SOUND=OFF \
	-DWITH_STATIC=ON \
	-DWITH_STATIC_DRIVERS=ON \
	-DWITH_DRIVER_OPENGL=OFF \
	-DWITH_DRIVER_OPENAL=OFF \
	-DWITH_NEL_TESTS=OFF \
	-DWITH_NEL_TOOLS=OFF \
	-DWITH_NEL_SAMPLES=OFF \
	../code/

$? | time make -j12

#	-DWITH_RYZOM_TOOLS=OFF \
#	-DWITH_NEL_TOOLS=OFF \

# Possible settings:
# ========================
#	-DWITH_NEL_SAMPLES=OFF \
#	-DWITH_NEL_TOOLS=OFF \
#	-DWITH_RYZOM_CLIENT=OFF \
#	-DWITH_RYZOM_SERVER=OFF \
#	-DWITH_RYZOM_TOOLS=OFF \
#	-DWITH_STATIC_DRIVERS=ON \
#	-DWITH_SYMBOLS=ON \
#	-DWITH_NEL_TESTS=OFF \
#	-DCMAKE_BUILD_TYPE=Debug \
#
#	-DWITH_LUA51=OFF \
#
#	-DWITH_NEL=ON \
#	-DWITH_SOUND=OFF \
#
# Server related:
# ===============
#	-DFINAL_VERSION=ON \
#	-DWITH_STATIC=ON \
#	-DWITH_QT=OFF \
#
#	-DWITH_DRIVER_OPENGL=OFF \
#	-DWITH_DRIVER_OPENAL=OFF \
