#!/bin/bash
# Takes one argument which is optional, the number of processes for gmake spawn.
# If omitted, defaults to: number of cores in build-host + 50%
#
# To build:
# 1) Plaece this script anywhere, alongside with Ryzom directory "code/" is
#    recommended as it's tested.
# 2) Make a directory *alongside* with the code/ directory. Name does not
#    matter. Example:
#    mkdir RYZOM_SERVER_BUILD
#    cd RYZOM_SERVER_BUILD
# 3) Execute script with or without argument.
# 4) Make a link to build. Server setup and shard script expects build be be
#    at a certain path. I recommend not to build in source, It's wrong in so
#    many ways, among others it does not permit you to try different builds,
#    pollutes source directory with intermediate files e.t.c. However to
#    satisfy the final scripts do the following:
#
#    cd ../code
#    ln -s RYZOM_SERVER_BUILD build
#
#  Voilá! 
#
# Some good references:
# https://ryzomcore.atlassian.net/wiki/display/RC/CMake+Options
# https://ryzomcore.atlassian.net/wiki/display/RC/Configure+Linux+Server

PROPS=$(cat /proc/cpuinfo | grep processor | wc -l)
JDEFAULT=$(( ${PROPS} + ${PROPS}/2 ))
export DISTCC_HOSTS='localhost' # If distcc is used, set this to the hosts in
								# cluster
TS=$(date "+%Y-%m-%d.%H.%M.%S")
LOG_FILE=/tmp/ryzon_build_server.log
if [ -f $LOG_FILE ]; then
	OLDLOG=${LOG_FILE}.${TS}
	mv $LOG_FILE $OLDLOG
fi
JN=${1-${JDEFAULT}}

cd $(dirname $0)
RROOT=$(pwd)
cd $OLDPWD

cmake \
	-DWITH_RYZOM_SERVER=ON \
	-DWITH_RYZOM_CLIENT=OFF \
	-DFINAL_VERSION=OFF \
	-DWITH_QT=OFF \
	-DWITH_STATIC=ON \
	-DWITH_STATIC_DRIVERS=ON \
	-DWITH_DRIVER_OPENGL=OFF \
	-DWITH_DRIVER_OPENAL=OFF \
	-DWITH_NEL_TESTS=OFF \
	-DWITH_NEL_TOOLS=OFF \
	-DCMAKE_BUILD_TYPE=Debug \
	../code/
	
#	-DCMAKE_CXX_COMPILER="${RROOT}/g++" \
#	-DCMAKE_C_COMPILER="${RROOT}/gcc" \
#	-DWITH_SYMBOLS=ON \

test $? && ( time make -j${JN} 2>&1) | tee ${LOG_FILE}
test -z $OLDLOG && echo "Old log-file: $OLDLOG"
echo
echo "New log-file: $LOG_FILE"
echo "If you have grcat, look at log like this:"
echo "cat $LOG_FILE | grcat conf.gcc | cat -rS"

#	-DWITH_RYZOM_TOOLS=OFF \
#	-DWITH_NEL_TOOLS=OFF \

# Possible settings:
# ========================
#	-DWITH_NEL_SAMPLES=OFF \
#	-DWITH_NEL_TOOLS=OFF \
#	-DWITH_RYZOM_CLIENT=OFF \
#	-DWITH_RYZOM_SERVER=OFF \
#	-DWITH_RYZOM_TOOLS=OFF \
#	-DWITH_STATIC_DRIVERS=ON \
#	-DWITH_SYMBOLS=ON \
#	-DWITH_NEL_TESTS=OFF \
#	-DCMAKE_BUILD_TYPE=Debug \
#
#	-DWITH_LUA51=OFF \
#
#	-DWITH_NEL=ON \
#	-DWITH_SOUND=OFF \
#
# Server related:
# ===============
#	-DFINAL_VERSION=ON \
#	-DWITH_STATIC=ON \
#	-DWITH_QT=OFF \
#
#	-DWITH_DRIVER_OPENGL=OFF \
#	-DWITH_DRIVER_OPENAL=OFF \
